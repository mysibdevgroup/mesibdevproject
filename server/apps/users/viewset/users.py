from rest_framework.authtoken.models import Token
from rest_framework.viewsets import GenericViewSet
from rest_framework import mixins, status
from rest_framework.authtoken.views import ObtainAuthToken
from django.contrib.auth.models import User
from apps.users.serializer import UserSerializer
from rest_framework.response import Response

class UserViewSet(mixins.CreateModelMixin,
                  GenericViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        token, created = Token.objects.get_or_create(user=serializer.instance)
        return Response({'user': serializer.data, 'token': token.key}, status=status.HTTP_201_CREATED)


class LoginViewSet(ObtainAuthToken):
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({'user': serializer.data, 'token': token.key})






