from rest_framework import routers

from apps.test.viewsets import TestViewSet
from apps.users.viewset import UserViewSet

router = routers.DefaultRouter()
router.register('test', TestViewSet, base_name='test')
router.register('user', UserViewSet, base_name='user')
